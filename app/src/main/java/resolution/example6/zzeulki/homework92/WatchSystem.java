package resolution.example6.zzeulki.homework92;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Zzeulki on 16. 11. 5..
 */
public class WatchSystem extends BroadcastReceiver {
    int mCount = 0;
    public WatchSystem() { }
    @Override
    public void onReceive(Context context, Intent intent) {
// TODO: This method is called when the BroadcastReceiver is receiving // an Intent broadcast.
        String act = intent.getAction();
        mCount++;
        if (act.equals(Intent.ACTION_BATTERY_CHANGED)) {

            Toast.makeText(context,"BR" + mCount + ": ACTION_BATTERY_CHANGED "
                    + intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0) + "%",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem",
                "BR" + mCount + ": ACTION_BATTERY_CHANGED "
                        + intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0) + "%");
        }
        else if (act.equals(Intent.ACTION_BATTERY_LOW)) {

            Toast.makeText(context,"BR" + mCount + ": ACTION_BATTERY_LOW",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem", "BR" + mCount + ": ACTION_BATTERY_LOW");
        }
        else if (act.equals(Intent.ACTION_BATTERY_OKAY)) {

            Toast.makeText(context,"BR" + mCount + ": ACTION_BATTERY_OKAY",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem", "BR" + mCount + ": ACTION_BATTERY_OKAY");
        }
        else if (act.equals(Intent.ACTION_POWER_CONNECTED)) {

            Toast.makeText(context,"BR" + mCount + ": ACTION_POWER_CONNECTED",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem", "BR" + mCount + ": ACTION_POWER_CONNECTED"); }
        else if (act.equals(Intent.ACTION_POWER_DISCONNECTED)) {

            Toast.makeText(context,"BR" + mCount + ": ACTION_POWER_DISCONNECTED",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem", "BR" + mCount + ": ACTION_POWER_DISCONNECTED");
        }
        else if (act.equals(Intent.ACTION_MEDIA_MOUNTED)) {

            Toast.makeText(context, "BR" + mCount + ": ACTION_MEDIA_MOUNTED",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem", "BR" + mCount + ": ACTION_MEDIA_MOUNTED");

        }
        else if (act.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {

            Toast.makeText(context,"BR" + mCount + ": ACTION_MEDIA_UNMOUNTED",Toast.LENGTH_SHORT).show();

            Log.i("WatchSystem", "BR" + mCount + ": ACTION_MEDIA_UNMOUNTED");
        }
    }
}